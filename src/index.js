import fromFunction from "callbag-from-function";
import H from "callbag-html";
import { of, fromEvent } from "rxjs";
import { startWith, switchMap, tap, share } from "rxjs/operators";
import { draw$, move$ } from "./modes";
import { drawOnCanvas } from "./rendering";
import { switchScan, toObservable } from "./util";
import { ui } from "./rendering";

export class DrawingBoard extends HTMLElement {
  static get observedAttributes() {
    return ["canvas-width", "canvas-height"];
  }
  get canvasWidth() {
    return this.getAttribute("canvas-width");
  }

  set canvasWidth(v) {
    this.setAttribute("canvas-width", v);
  }
  get canvasHeight() {
    return this.getAttribute("canvas-height");
  }
  set canvasHeight(v) {
    this.setAttribute("canvas-height", v);
  }
  constructor() {
    super();
    const shadowRoot = this.attachShadow({ mode: "open" });
    shadowRoot.appendChild(H.style(`
    :host {
      display: flex;
      flex-flow: column;
      border: 0.1em solid black;
      width: 100%;
      min-width: 320px;
      max-width: 27em;
    }
    `));

    const { source, emitter } = fromFunction();
    const source$ = toObservable(source);

    const context = H.canvas({
      width: this.canvasWidth || 360,
      height: this.canvasHeight || 255
    }).getContext("2d");
    const { width, height } = context.canvas;

    const initialState = {
      drawMode: "MOVE",
      drawSpeed: 50,
      pointDistance: 10,
      pointSize: 4,
      points: [
        { x: 0, y: height / 2 },
        { x: width, y: height / 2 }
      ],
      context,
      emitter
    }

    shadowRoot.appendChild(ui(initialState));

    const state$ = source$.pipe(
      startWith(initialState),
      switchScan((acc, v) => of({ ...acc, ...v }).pipe(
        switchMap(state => {
          return ({
            "DRAW": draw$,
            "MOVE": move$,
          })[state.drawMode](state);
        }),
      ), initialState),
      share()
    );

    state$.subscribe(drawOnCanvas);
  }
}

if (customElements && customElements.get("drawing-board") === undefined)
  customElements.define("drawing-board", DrawingBoard);
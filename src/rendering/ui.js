import H from "callbag-html";

export const ui = state => {

  const { context: { canvas }, drawMode, drawSpeed, pointDistance, pointSize, emitter } = state;
  const { width, height } = canvas;
  canvas.style.border = "1px solid black";
  canvas.style.width = "100%";
  canvas.style.height = "auto";

  return H.div({
    style: {
      display: "flex",
      flexFlow: "column",
      width: "calc(100%-3em)",
      padding: "1em",
    }
  }, [
      canvas,
      H.div({
        id: "controls",
        style: {
          margin: "1em 0em",
          display: "flex",
          flexFlow: "row wrap",
          // alignItems: "center",
        }
      }, [
        H.fieldset({
          style: {
            maxWidth: "10em",
            margin: "1em 0em",
          }
        },
          [
            H.legend("Mouse Pointer Mode"),
            H.div({
              onchange: ev => (uncheckOthers(ev.target), emitter({ drawMode: ev.target.value })),
            },
              ["MOVE", "DRAW"].map(DRAWMODE =>
                H.div([
                  H.input({
                    type: "radio",
                    value: DRAWMODE,
                    checked: DRAWMODE === drawMode ? true : false
                  }),
                  H.label(drawModeToLabel(DRAWMODE))
                ])
              )
            )
          ]),
        H.button({
          type: "submit",
          style: {
            margin: "2em 1em",
            maxWidth: "10em",
            maxHeight: "2em"
          },
          onclick: () => {
            emitter({
              points: [
                { x: 0, y: height / 2 },
                { x: width, y: height / 2 }
              ]
            });
          }
        }, "Erase All"),
        range({
          min: 0,
          max: 500,
          step: 5,
          id: "drawSpeed",
          drawSpeed,
          oninput: ev => emitter({ drawSpeed: parseInt(ev.target.value) })
        }),
        range({
          min: 0,
          max: 40,
          step: 1,
          id: "pointDistance",
          pointDistance,
          oninput: ev => emitter({ pointDistance: parseInt(ev.target.value) })
        }),
        range({
          min: 0,
          max: 10,
          step: 1,
          id: "pointSize",
          pointSize,
          oninput: ev => emitter({ pointSize: parseInt(ev.target.value) })
        })
      ])]);

}

const drawModeToLabel = drawMode => ({
  "MOVE": "Single Point",
  "DRAW": "Multi Point",
})[drawMode]

const idToFriendly = id => ({
  "drawSpeed": "Drawing Speed",
  "pointDistance": "Point Distance",
  "pointSize": "Point Size",
})[id];

const range = ({ min, max, step, id, oninput, value }) => H.div([
  H.input({
    id,
    type: "range",
    min,
    max,
    step,
    oninput,
    value
  }),
  H.label(idToFriendly(id))
]);

function uncheckOthers(t) {
  Array.from(t.parentElement.parentElement.querySelectorAll("input[type=radio]"))
    .filter(x => x !== t)
    .forEach(x => x.checked = false)
}
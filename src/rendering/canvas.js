import Spline from "cubic-spline";

export const drawOnCanvas = state => {
  let { context, points } = state;
  const { canvas: { width, height }} = context;

  context.clearRect(0, 0, width, height);
  points.forEach(({ path }) => { if (path) context.stroke(path) } );
  context.beginPath();
  context.moveTo(0, height / 2);

  const xs = points.map(({ x }) => x);
  const ys = points.map(({ y }) => y);
  const spline = new Spline(xs, ys);

  let waveData = [];
  for (let x = 1; x <= width; x++) {
    let y = spline.at(x);
    context.lineTo(x, y);
    waveData.push([x,y]);
  }
  context.lineTo(width, height / 2);
  context.stroke();
  context.canvas.dispatchEvent(new CustomEvent("waveData", {
    detail: {
      waveData,
      height
    },
    bubbles: true,
    composed: true
  }))
}
import { makePoint, indexInsertedPoint, addMovementToStateWithPointer, switchScan } from "../util";
import { fromEvent, of } from "rxjs";
import { scan, startWith, takeUntil, map, switchMap } from "rxjs/operators";

export const move$ = state => {

  const canvasOn = ev => fromEvent(state.context.canvas, ev);

  return canvasOn("mousedown").pipe(
    switchScan((acc, downEvent) => {
      let { points, context, context: { canvas }, ...rest } = acc;
      let { offsetX, offsetY } = downEvent;
      let { x, y } = { x: offsetX * canvas.width / canvas.clientWidth, y: offsetY * canvas.height / canvas.clientHeight };
      let pointer = acc.points.findIndex(({ path }) => {
        if (!path) return false;
        return context.isPointInPath(path, x, y)
      });
      let seed = null;
      if (pointer === -1) {
        let newPoint = makePoint({ x, y }, state.pointSize);
        let { points: newPoints, pointer } = indexInsertedPoint(points, newPoint, state.pointSize);
        seed = { state: { points: newPoints, context, ...rest }, pointer };
      } else {
        seed = { state: { points, context, ...rest }, pointer };
      }

      return of(seed).pipe(
        switchMap((seed) =>
          canvasOn("mousemove").pipe(
            scan((stateWithPointer, moveEvent) => {
              let X = addMovementToStateWithPointer(stateWithPointer)(moveEvent);
              return X;
            }, seed),
            startWith(seed),
            takeUntil(canvasOn("mouseup")),
          )
        ),
        map(({ state }) => state),
      );
    }, state),
    startWith(state)
  );
}
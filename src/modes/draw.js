import { fromEvent } from "rxjs";
import { flatMap, map, scan, startWith, takeUntil, tap, throttleTime } from "rxjs/operators";
import { insertPoint, makePoint } from "../util";

export const draw$ = state => {

  const { context: { canvas }, points, drawSpeed, pointDistance, pointSize } = state;
  const canvasOn = ev => fromEvent(canvas, ev);

  return canvasOn("mousedown").pipe(
    flatMap(downEvent => canvasOn("mousemove").pipe(
      startWith(downEvent),
      takeUntil(canvasOn("mouseup")),
    )),
    throttleTime(drawSpeed),
    map(({ offsetX, offsetY }) => ({ x: offsetX * canvas.width / canvas.clientWidth, y: offsetY * canvas.height / canvas.clientHeight })),
    scan((acc, { x, y }) =>
      insertPoint(acc, makePoint({ x, y }, pointSize), pointDistance),
      points),
    map(points => ({ ...state, points })),
    startWith(state)
  );
}
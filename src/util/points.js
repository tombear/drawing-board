export const makePoint = ({ x, y }, pointSize) => {
  let path = new Path2D();
  path.arc(x, y, pointSize, 0, Math.PI * 180);
  return { x, y, path };
}

export const insertPoint = (oldPoints, newPoint, pointDistance) => {
  const newPoints = oldPoints.filter(p => Math.abs(newPoint.x - p.x) > pointDistance);
  return [...newPoints, newPoint].sort((a, b) => a.x - b.x);
}

export const popAtIndex = (xs, i) => {
  if (i < 0 || xs[i] === undefined) return [...xs];
  return [...xs.slice(0, i), ...xs.slice(i + 1)];
}

export const indexInsertedPoint = (points, { x, y }, pointSize) => {
  const newPoint = makePoint({ x, y }, pointSize);
  const newPoints = [...points, newPoint].sort((a, b) => a.x - b.x);
  const pointer = newPoints.indexOf(newPoint);
  return { points: newPoints, pointer };
}

export const addMovementToStateWithPointer = ({ state, state: { context: { canvas }}, pointer }) => ({ movementX, movementY }) => {
  let point = { ...state.points[pointer] };
  let points = popAtIndex([...state.points], pointer);
  point.x += movementX * canvas.width / canvas.clientWidth;
  point.y += movementY * canvas.height / canvas.clientHeight;
  let { points: newPoints, pointer: newPointer } = indexInsertedPoint(points, point, state.pointSize);
  return { state: { ...state, points: newPoints }, pointer: newPointer };
}

import commonjs from "rollup-plugin-commonjs";
import nodeResolve from "rollup-plugin-node-resolve";
import babel from 'rollup-plugin-babel';

export default {
  input: "./src/index.js",
  output: {
    file: "./dist/drawing-board.js",
    format: "esm",
    sourcemap: true,
    name: "DrawingBoard"
  },
  plugins: [
    nodeResolve(),
    babel({ exclude: 'node_modules/**' }),
    commonjs(),
  ]
};